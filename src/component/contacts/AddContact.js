import React, { Component } from 'react';
import { Consumer } from '../../Context';
import uuid from 'uuid';
import TextInputGroup from '../layout/TextInputGroup';
 class AddContact extends Component {
     state={  
         
         name:'',
         email:'',
         phone:'',
         errors:{
             
         }
         
     };
     onSubmit=(dispatch, e)=>{
        e.preventDefault();
        const { name, email, phone}=this.state;
        //check error
        if(name===''){
            this.setState({
                errors:{name:'Name is required'
            }});
            return;
        } 

        if(email===''){
            this.setState({
                errors:{email:'Email is required'
            }});
            return;
        } 

        if(phone===''){
            this.setState({
                errors:{phone:'Phone is required'
            }});
            return;
        } 
        const newContact ={ 
            id: uuid(),
            name,
            email,
            phone
        }
        dispatch({type: 'ADD_CONTACT', payload: newContact}) ;
        this.setState({
            name:'',
            email:'',
            phone:'',
            errors:{}
        });
        this.props.history.push('/');
    };

     onChange=(e)=>this.setState({[e.target.name]: e.target.value});
    
  render() {
      const {name, email, phone, errors}=this.state;
      return (
          <Consumer>
              {value => {
                  const { dispatch }= value;
                  return (

                    <div className="card mb-3">
      <div className="card-header">
      Add Contacts
      <div className="card-body">
      <form onSubmit={this.onSubmit.bind(this, dispatch)}> 
          {/* <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
          type="text"
          name="name"
          className="form-control form-control-lg"
          placeholder="Enter Name...."
          value={name}
          onChange={this.onChange}
          />
          </div>
          <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
          type="email"
          name="email"
          className="form-control form-control-lg"
          placeholder="Enter Email...."
          value={email}
          onChange={this.onChange}
          />
          </div>
          <div className="form-group">
          <label htmlFor="phone">Phone</label>
          <input
          type="text"
          name="phone"
          className="form-control form-control-lg"
          placeholder="Enter Phone No...."
          value={phone}
          onChange={this.onChange}
          />
          </div> */}
          <TextInputGroup
          label="name"
          name="name"
          placeholder="Enter Name"
          value={name}
          onChange={this.onChange}
          error={errors.name}
          />

          <TextInputGroup
          label="Email"
          name="email"
          type="email"
          placeholder="Enter Email"
          value={email}
          onChange={this.onChange}
          error={errors.email}
          />

          <TextInputGroup
          label="Phone"
          name="phone"
          placeholder="Enter Phone"
          value={phone}
          onChange={this.onChange}
          error={errors.phone}
          />
          
          <input 
          type="submit"
           value="Add Contact"
         className="btn btn-block btn-light"></input>
      </form>
      </div>

      </div>
        
      </div>
                  )
              }}
          </Consumer>
      )
   
  }
}
export default AddContact;
