import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Consumer} from '../../Context';


export default class Contact extends Component {
    state={
        showInfo: false
    };
   
    onDeleteClick = (id ,dispatch) =>{
        dispatch({type: 'DELETE_CONTACT', payload: id});
        
    }
  render() {
    const { id, name, email, phone} = this.props.contact;
    const { showInfo} = this.state;
    return ( 

            <Consumer>
                {value =>{
                    const { dispatch } = value;
                    return(
                        <div className="card card-body mb-3">
        <h6>{name}<i className="fas fa-sort-down"
         onClick={()=>this.setState({showInfo: !this.state.showInfo})}
         style={{cursor:'pointer', color:'blue'}}
         />
         <i className="fas fa-times"
         style={{cursor:'pointer', float:'right', color:'red'}}
         onClick ={this.onDeleteClick.bind(this, id,  dispatch)}
          />
         </h6>
          {showInfo ? (<ul className="list-group">
              <li className="list-group-item">{email}</li>
              <li className="list-group-item">{phone}</li>
              
          </ul>) :null}
          
      </div>

                    )
                }}

            </Consumer>
        
    );
  }
}
  Contact.PropTypes ={
      contact:PropTypes.object.isRequired
    
  };