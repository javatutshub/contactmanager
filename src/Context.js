import React, { Component } from 'react';

const Context= React.createContext(); 
  
const reducer=(state, action)=>{
    switch(action.type){
        case 'DELETE_CONTACT':
        return{
            ...state,
            contacts:state.contacts.filter(contact =>
                contact.id !== action.payload)
        };
        case 'ADD_CONTACT':
        return{
            ...state,
           contacts: [action.payload, ...state.contacts]
        };
        default:
        return state;
    }
}

export  class Provider extends Component {
    state={
        contacts:[ 
            {
                id:1,
                name:'PRAMOD KUMAR',
                email:'kumar@gmail.com',
                phone:'7987416155'
            },
            {
                id:2,
                name:'RAJ KUMAR',
                email:'Raj@gmail.com',
                phone:'7987416155'
            },
            {
                id:1,
                name:'GHANENDRA YADAV',
                email:'yadav@gmail.com',
                phone:'7987416155'
            },
            {
                id:2,
                name:'SANJAY SINGH',
                email:'singh@gmail.com',
                phone:'7987416155'
            }

        ],
        dispatch: action =>this.setState(state => reducer(state, action))
        
    };
   render(){
       return(
           <Context.Provider value={this.state}>
           {this.props.children}
           </Context.Provider>
       )
       
   }
}
export const Consumer =Context.Consumer;